<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Inertia\Inertia;

class UserController extends Controller
{
    /**
     * Lista de usuarios.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return Inertia::render('Users', ['users' => $users]);
    }

    /**
     * Posts del usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function posts($id)
    {
        $user = User::find($id);
        $posts = $user->posts()->get();
        return Inertia::render('Posts', ['posts' => $posts]);
    }
}
