<?php

namespace App\Console\Commands;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class CreateComment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:comment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Selecciona un post aleatoriamente y cree un comentario';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $post = Post::inRandomOrder()->limit(1)->first();
        $url = 'https://jsonplaceholder.typicode.com/posts/'.$post->id.'/comments';
        $response = Http::get($url);
        if ($response->successful()){
            $comments = $response->json();
            $randomKey = array_rand($comments);
            $randomComment = $comments[$randomKey];
            $comment = new Comment();
            $comment->name = $randomComment['name'];
            $comment->email = $randomComment['email'];
            $comment->body = $randomComment['body'];
            $post->comments()->save($comment);
        }
        return 0;
    }
}
