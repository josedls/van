<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;

class ImportUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Inserta todos los usuarios en la base de datos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = 'https://jsonplaceholder.typicode.com/users';
        $response = Http::get($url);
        if ($response->successful()){
            $users = $response->json();
            foreach($users as $us){
                $user = new User();
                $user->id = $us['id'];
                $user->name = $us['name'];
                $user->username = $us['username'];
                $user->email = $us['email'];
                $user->password = Hash::make(Str::random(8));
                $user->address = $us['address']['street'].' '.$us['address']['suite'].' '.$us['address']['city'];
                $user->phone = $us['phone'];
                $user->website = $us['website'];
                $user->save();
            }
        }
        return 0;
    }
}
