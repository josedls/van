<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'userId',
        'title',
        'body',
    ];

    public function user(){
        return $this->belongsTo("User", 'userId', 'id');
    }

    public function comments(){
        return $this->hasMany(Comment::class, 'postId', 'id');
    }
}
