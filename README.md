# VAN

Interview Challenge

## Installation

1. Run in your terminal:

```bash
git clone https://josedls@bitbucket.org/josedls/van.git van
composer install
npm ci
npm run dev
cp .env.example .env
php artisan key:generate
php artisan migrate
php artisan db:seed
```

## Usage
1. Credentials: 
* Username: admin@admin.com
* Password: password

2. For import users

```bash
php artisan import:users
```

3. For import posts
```bash
php artisan import:posts
```

4. For schedule tasks
```bash
php artisan schedule:work
```

## Thanks
Jose De los Santos